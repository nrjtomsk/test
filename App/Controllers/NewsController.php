<?php

namespace App\Controllers;

use App\Repository\Exception\ImageRepositoryException;
use App\Repository\ImageRepository;
use App\Repository\NewsRepository;
use App\Service\ImageService;
use Core\Redirect;
use Core\Request;
use \Core\View;
use \Core\Controller;

/**
 * Class Home
 * @package App\Controllers
 */
class NewsController extends Controller
{
    public function indexAction()
    {
        $newsRepository = new NewsRepository();
        $data = $newsRepository->getAll();

        View::renderTemplate('Home/index.html', ['data' => $data]);
    }

    public function viewAction($id)
    {
        $newsRepository = new NewsRepository();
        $news = $newsRepository->getById($id);
        View::renderTemplate('Home/view.html', ['news' => $news]);
    }

    public function createAction()
    {
        View::renderTemplate('Home/create.html');
    }

    public function saveAction()
    {
        $imageRepository = new ImageRepository();
        $newsRepository = new NewsRepository();
        $imageServece = new ImageService();

        $data = Request::getPostData();
        $file = Request::getPostFile();
        $url = $imageServece->uploadImage($file);
        $id = $imageRepository->addImageInDB($url);
        $data['image_id'] = $id;
        $newsId = $newsRepository->create($data);

        $this->sendJSONAnswer(['news_id' => $newsId]);
    }
    
    public function editPageAction($id)
    {
        $newsRepository = new NewsRepository();
        $news = $newsRepository->getById($id);
        View::renderTemplate('Home/edit.html', ['news' => $news]);
    }

    public function editAction($id)
    {
        $newsRepository = new NewsRepository();
        $imageRepository = new ImageRepository();
        $imageServece = new ImageService();

        $data = Request::getPostData();
        $file = Request::getPostFile();

        if(!empty($file)) {
            $url = $imageServece->uploadImage($file);
            $imageId = $imageRepository->addImageInDB($url);
            $data['image_id'] = $imageId;
        } else {
            $data['image_id'] = null;
        }

        $newsRepository->edit($id, $data);

        $this->sendJSONAnswer(['news_id' => $id]);
    }

    public function deleteAction($id)
    {
        $newsRepository = new NewsRepository();
        $imageRepository = new ImageRepository();

        $news = $newsRepository->getById($id);
        try {
            $imageRepository->delete($news->getImageId());
        } catch (ImageRepositoryException $e) {

        }

        $newsRepository->delete($news->getId());
        $this->indexAction();
    }

}
