<?php

namespace App\Repository;


use App\Repository\Exception\NewsRepositoryException;
use Core\Repository;
use App\Entity\News;

/**
 * Class News
 * @package App\Models
 */
class NewsRepository extends Repository
{

    /** @var \PDO */
    public $db;

    /**
     * NewsRepository constructor.
     */
    public function __construct()
    {
        $this->db = static::getDB();;
    }

    /**
     * @return array | null
     */
    public function getAll()
    {
        $stmt = $this->db->query('SELECT * FROM News');
        return $this->prepareArrayData($stmt->fetchAll());
    }

    /**
     * @param int $id
     * @return News
     */
    public function getById(int $id) : News
    {
        $stmt = $this->db->prepare('SELECT * FROM News WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->execute();

        return $this->prepareData($stmt->fetch());
    }


    /**
     * @param array $data
     * @return int
     */
    public function create(array $data) : int
    {
        $now =  new \DateTime('now');
        $nowString = $now->format('Y-m-d');

        $stmt = $this->db->prepare('INSERT INTO news (title, text, image_id, updated_at) VALUES (:title, :text, :image_id, :updated_at)');
        $stmt->bindParam(':title', $data['title']);
        $stmt->bindParam(':text', $data['text']);
        $stmt->bindParam(':image_id', $data['image_id']);
        $stmt->bindParam(':updated_at', $nowString);
        $stmt->execute();

        return $this->db->lastInsertId();
    }

    /**
     * @param int $id
     * @param array $data
     */
    public function edit(int $id, array $data)
    {
        $now =  new \DateTime('now');
        $nowString = $now->format('Y-m-d');

        $stmt = $this->db->prepare('UPDATE News SET title = :title, 
                                                    text = :text, 
                                                    image_id = :image_id,
                                                    updated_at = :updated_at
                                                    WHERE id = :id');
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':title', $data['title']);
        $stmt->bindParam(':text', $data['text']);

        if($data['image_id'] !== null) {
            $stmt->bindParam(':image_id', $data['image_id']);
        } else {
            $news = $this->getById($id);
            $imageID = $news->getImageId();
            $stmt->bindParam(':image_id', $imageID);
        }

        $stmt->bindParam(':updated_at', $nowString);
        $stmt->execute();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        $stmt = $this->db->prepare('DELETE FROM News WHERE id = :id');
        $stmt->bindParam(':id', $id);


        return $stmt->execute();
    }

    /**
     * @param array $data
     * @return array | null
     * @throws NewsRepositoryException
     */
    private function prepareArrayData(array $data)
    {
        if(empty($data)) {
            return null;
        }

        $result = [];
        foreach ($data as $item)
        {
            $news = new News();
            $news->setId($item['id'])
                 ->setTitle($item['title'])
                 ->setText($item['text'])
                 ->setUpdatedAt($item['updated_at'])
                 ->setImageId($item['image_id'])
                 ->setImageUrl();
            array_push($result, $news);
        }

        return $result;
    }

    /**
     * @param array $data
     * @return News
     * @throws NewsRepositoryException
     */
    private function prepareData(array $data) : News
    {
        if(empty($data)) {
            throw new NewsRepositoryException('No data');
        }
        $news = new News();

        return  $news->setId($data['id'])
                     ->setTitle($data['title'])
                     ->setText($data['text'])
                     ->setUpdatedAt($data['updated_at'])
                     ->setImageId($data['image_id'])
                     ->setImageUrl();
    }

}