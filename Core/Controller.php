<?php

namespace Core;
use Core\Helper\ClassHelper;

/**
 * Class Controller
 * @package Core
 */
abstract class Controller
{
    public $view;

    public function __construct()
    {
        $name = ClassHelper::getClassName($this);
        $this->view = new View($name);
    }

    public function refresh()
    {
        header("Refresh:0");
    }

    public function sendJSONAnswer(array $data)
    {
        print_r(json_encode($data));
    }
}
